import React from 'react';
import { BrowserRouter as Router, Switch, Route } from 'react-router-dom';
import NavMenu from './components/NavMenu';
import Admin from './components/Admin';
import Dashboard from './components/Dashboard';
import Home from './components/Home';
import VistaPrevia from './components/VistaPrevia';
import BusquedaEmpresa from './components/BusquedaEmpresa';
import BusquedaCodigo from './components/BusquedaCodigo';
import ImportarDatos from './components/ImportarDatos';

function App() {
  return (
    <div>
      <Router>
        <NavMenu></NavMenu>
        <Switch>
          <Route exact path='/' component={Home}></Route>
          <Route path='/admin' component={Admin}></Route>
          <Route path='/dashboard' component={Dashboard}></Route>
          <Route path='/vistaprevia' component={VistaPrevia}></Route>
          <Route path='/busqueda_empresa' component={BusquedaEmpresa}></Route>
          <Route path='/busqueda_codigo' component={BusquedaCodigo}></Route>
          <Route path='/importar' component={ImportarDatos}></Route>
        </Switch>
      </Router>
    </div>
  );
}

export default App;
