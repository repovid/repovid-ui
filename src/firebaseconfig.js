import firebase from 'firebase';
import 'firebase/auth';

const firebaseConfig = {
    apiKey: "AIzaSyC0mmT56pYMn5K2AuN6ggeqh94LYEb5o1A",
    authDomain: "repovid-2ba4e.firebaseapp.com",
    projectId: "repovid-2ba4e",
    storageBucket: "repovid-2ba4e.appspot.com",
    messagingSenderId: "652294370031",
    appId: "1:652294370031:web:868ed03a1644ac3b28a11a",
    measurementId: "G-VNB1BJ2CWN"
};
// Initialize Firebase
const fire = firebase.initializeApp(firebaseConfig);
const auth = fire.auth()

export {auth}
