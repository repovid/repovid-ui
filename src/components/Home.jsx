import React, { useEffect, useState } from 'react';
import { auth } from '../firebaseconfig';
import Login from './Login';
import RecepcionUser from './RecepcionUser';

const Home = () => {

    const [user, setUser] = useState(null)

    useEffect(() => {
        auth.onAuthStateChanged((user) => {
            if (user) {
                setUser(user.email)
            }
        })
    }, [])


    return (
        <div className='text-center'>
            {
                user ?
                    (
                        <RecepcionUser email={user}></RecepcionUser>
                    )
                    :
                    (
                        <Login></Login>
                    )
            }
        </div>
    )
}

export default Home