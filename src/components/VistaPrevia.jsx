import React, { useState, useEffect } from 'react';
import { DataTable } from 'primereact/datatable';
import { DataScroller } from 'primereact/datascroller';
import { Column } from 'primereact/column';
import { Panel } from 'primereact/panel';
import axios from 'axios';

//styles
import 'primereact/resources/themes/saga-blue/theme.css';
import 'primereact/resources/primereact.min.css';
import 'primeicons/primeicons.css';

const VistaPrevia = (props) => {

    const data = props.history.location.state

    const [exportaciones, setExportaciones] = useState([])
    const [msn, setMsn] = useState('Cargando datos. Esto puede tardar un poco.')

    const downloadFile = (res) => {
        const blob = new Blob([res.data])
        const link1 = document.createElement('a')
        link1.href = window.URL.createObjectURL(blob)
        link1.download = 'datosRepovid.xlsx'
        link1.click()
    }

    useEffect(async () => {
        if (data.type === 'arancel') {
            await axios
                .get('http://3.235.143.253:8080/export/codArancel', {
                    params: {
                        'codigoArancel': data.codigo,
                        'paisOrigen': data.pais,
                        'year': data.anio
                    }
                }).then(res => {
                    setMsn('Resultados encontrados: ' + res.data.length)
                    setExportaciones(res.data)

                })
        }
        if (data.type === 'empresa') {
            await axios
                .get('http://3.235.143.253:8080/export/idEmpresa', {
                    params: {
                        'rut': data.codigo,
                        'paisOrigen': data.pais,
                        'year': data.anio
                    }
                }).then(res => {
                    setMsn('Resultados encontrados: ' + res.data.length)
                    setExportaciones(res.data)

                })
        }

        await axios
            .get('http://3.235.143.253:8080/import/test').then(res => {
                console.log(res.data)
            })


    }, [])

    const exportar = async () => {
        alert('Se está preparando el archivo para descargar. Al momento de estar listo se descargará automáticamente en su navegador.')
        if (data.type === 'arancel') {
            await axios
                .get('http://3.235.143.253:8080/export/workbookCod', {
                    responseType: 'blob',
                    params: {
                        'codigoArancel': data.codigo,
                        'paisOrigen': data.pais,
                        'year': data.anio
                    }
                }).then(res => {
                    downloadFile(res)
                })
        }

        if (data.type === 'empresa') {
            await axios
                .get('http://3.235.143.253:8080/export/workbookEmpresa', {
                    responseType: 'blob',
                    params: {
                        'rut': data.codigo,
                        'paisOrigen': data.pais,
                        'year': data.anio
                    }
                }).then(res => {
                    downloadFile(res)
                })
        }

    }

    return (
        <div className='text-center mt-5'>
            <h5>{msn}</h5>
            <button className='btn btn-info' onClick={exportar}>Exportar en Excel</button>
            <Panel header='Resultados de búsqueda'>
                {
                    data.pais === 'CHILE' ?
                        (
                            <DataTable value={exportaciones} paginator rows={10} rowsPerPageOptions={[10, 50, 100]}>
                                <Column field='codigoArancel' header='Código Arancel'></Column>
                                <Column field='ano' header='Año'></Column>
                                <Column field='paisDestino' header='Pais de Destino'></Column>
                                <Column field='nombreProducto' header='Producto'></Column>
                                <Column field='numeroAceptacion' header='Número de Aceptación'></Column>
                                <Column field='cifUnitario' header='CIF Unitario'></Column>
                                <Column field='fobUS' header='FOB'></Column>
                                <Column field='fleteUnitario' header='Flete Unitario'></Column>
                            </DataTable>
                        )
                        :
                        (
                            <span></span>
                        )
                }

                {
                    data.pais === 'ARGENTINA' ?
                        (
                            <DataTable value={exportaciones} paginator rows={10} rowsPerPageOptions={[10, 50, 100]}>
                                <Column field='ano' header='Año'></Column>
                                <Column field='paisDestino' header='Pais de Destino'></Column>
                                <Column field='codigoArancelario' header='Código Arancelario'></Column>
                                <Column field='numeroAceptacion' header='Número de Aceptación'></Column>
                                <Column field='valorAduanaTotal' header='Aduana Total'></Column>
                                <Column field='fobTotalUS' header='FOB Total US'></Column>
                            </DataTable>
                        )
                        :
                        (
                            <span></span>
                        )
                }

                {
                    data.pais === 'BOLIVIA' ?
                        (
                            <DataTable value={exportaciones} paginator rows={10} rowsPerPageOptions={[10, 50, 100]}>
                                <Column field='ano' header='Año'></Column>
                                <Column field='paisDestino' header='Pais de Destino'></Column>
                                <Column field='codigoArancel' header='Código Arancelario'></Column>
                                <Column field='tasaCambio' header='Tasa de Cambio'></Column>
                                <Column field='cifBsItem' header='CIF BS ITEM'></Column>
                                <Column field='fobUsItem' header='US FOB ITEM'></Column>
                            </DataTable>
                        )
                        :
                        (
                            <span></span>
                        )
                }

                {
                    data.pais === 'COLOMBIA' ?
                        (
                            <DataTable value={exportaciones} paginator rows={10} rowsPerPageOptions={[10, 50, 100]}>
                                <Column field='ano' header='Año'></Column>
                                <Column field='paisDestino' header='Pais de Destino'></Column>
                                <Column field='codigoArancel' header='Código Arancelario'></Column>
                                <Column field='fleteUS' header='Flete US'></Column>
                                <Column field='seguroUS' header='Seguro US'></Column>
                                <Column field='fobPesos' header='FOB Pesos'></Column>
                            </DataTable>
                        )
                        :
                        (
                            <span></span>
                        )
                }

                {
                    data.pais === 'COSTA RICA' ?
                        (
                            <DataTable value={exportaciones} paginator rows={10} rowsPerPageOptions={[10, 50, 100]}>
                                <Column field='ano' header='Año'></Column>
                                <Column field='paisDestino' header='Pais de Destino'></Column>
                                <Column field='codigoArancel' header='Código Arancelario'></Column>
                                <Column field='aduana2' header='Aduana de Destino'></Column>
                                <Column field='tasaCambio' header='Tasa de Cambio'></Column>
                                <Column field='fobUSITem' header='FOB US ITEM'></Column>
                            </DataTable>
                        )
                        :
                        (
                            <span></span>
                        )
                }

                {
                    data.pais === 'ECUADOR' ?
                        (
                            <DataTable value={exportaciones} paginator rows={10} rowsPerPageOptions={[10, 50, 100]}>
                                <Column field='ano' header='Año'></Column>
                                <Column field='paisDestino' header='Pais de Destino'></Column>
                                <Column field='codigoArancel' header='Código Arancelario'></Column>
                                <Column field='numeroManifiesto' header='Número Manifiesto'></Column>
                                <Column field='cantidad' header='Cantidad'></Column>
                                <Column field='fobUS' header='FOB US'></Column>
                            </DataTable>
                        )
                        :
                        (
                            <span></span>
                        )
                }

                {
                    data.pais === 'GUATEMALA' ?
                        (
                            <DataTable value={exportaciones} paginator rows={10} rowsPerPageOptions={[10, 50, 100]}>
                                <Column field='ano' header='Año'></Column>
                                <Column field='paisDestino' header='Pais de Destino'></Column>
                                <Column field='codigoArancel' header='Código Arancelario'></Column>
                                <Column field='pesoBruto' header='Peso Bruto'></Column>
                                <Column field='cantidad' header='Cantidad'></Column>
                                <Column field='fob' header='FOB'></Column>
                            </DataTable>
                        )
                        :
                        (
                            <span></span>
                        )
                }

                {
                    data.pais === 'MEXICO' ?
                        (
                            <DataTable value={exportaciones} paginator rows={10} rowsPerPageOptions={[10, 50, 100]}>
                                <Column field='ano' header='Año'></Column>
                                <Column field='paisDestino' header='Pais de Destino'></Column>
                                <Column field='codigoArancel' header='Código Arancelario'></Column>
                                <Column field='tasaCambio' header='Tasa de Cambio'></Column>
                                <Column field='fobMx' header='FOB MXN'></Column>
                                <Column field='fobUS' header='FOB US'></Column>
                            </DataTable>
                        )
                        :
                        (
                            <span></span>
                        )
                }

                {
                    data.pais === 'PANAMA' ?
                        (
                            <DataTable value={exportaciones} paginator rows={10} rowsPerPageOptions={[10, 50, 100]}>
                                <Column field='ano' header='Año'></Column>
                                <Column field='paisDestino' header='Pais de Destino'></Column>
                                <Column field='codigoArancel' header='Código Arancelario'></Column>
                                <Column field='cantidad' header='Cantidad'></Column>
                                <Column field='numeroLiquidacion' header='Número de Liquidación'></Column>
                                <Column field='fobUS' header='FOB US'></Column>
                            </DataTable>
                        )
                        :
                        (
                            <span></span>
                        )
                }

                {
                    data.pais === 'PARAGUAY' ?
                        (
                            <DataTable value={exportaciones} paginator rows={10} rowsPerPageOptions={[10, 50, 100]}>
                                <Column field='ano' header='Año'></Column>
                                <Column field='paisDestino' header='Pais de Destino'></Column>
                                <Column field='codigoArancel' header='Código Arancelario'></Column>
                                <Column field='cantidad' header='Cantidad'></Column>
                                <Column field='fleteUS' header='Flete US'></Column>
                                <Column field='fobUnitarioUS' header='FOB Unitario US'></Column>
                            </DataTable>
                        )
                        :
                        (
                            <span></span>
                        )
                }

                {
                    data.pais === 'PERU' ?
                        (
                            <DataTable value={exportaciones} paginator rows={10} rowsPerPageOptions={[10, 50, 100]}>
                                <Column field='ano' header='Año'></Column>
                                <Column field='paisDestino' header='Pais de Destino'></Column>
                                <Column field='codigoArancel' header='Código Arancelario'></Column>
                                <Column field='cantidad' header='Cantidad'></Column>
                                <Column field='fobUnitarioUS' header='FOB Unitario US'></Column>
                                <Column field='fobUS' header='FOB US'></Column>
                            </DataTable>
                        )
                        :
                        (
                            <span></span>
                        )
                }

                {
                    data.pais === 'URUGUAY' ?
                        (
                            <DataTable value={exportaciones} paginator rows={10} rowsPerPageOptions={[10, 50, 100]}>
                                <Column field='ano' header='Año'></Column>
                                <Column field='paisDestino' header='Pais de Destino'></Column>
                                <Column field='codigoArancel' header='Código Arancelario'></Column>
                                <Column field='cantidadComercial' header='Cantidad Comercial'></Column>
                                <Column field='fobUnitarioUS' header='FOB Unitario US'></Column>
                                <Column field='fobUSItem' header='FOB US ITEM'></Column>
                            </DataTable>
                        )
                        :
                        (
                            <span></span>
                        )
                }

                {
                    data.pais === 'VENEZUELA' ?
                        (
                            <DataTable value={exportaciones} paginator rows={10} rowsPerPageOptions={[10, 50, 100]}>
                                <Column field='ano' header='Año'></Column>
                                <Column field='paisDestino' header='Pais de Destino'></Column>
                                <Column field='codigoArancel' header='Código Arancelario'></Column>
                                <Column field='fobBols' header='FOB BOLS'></Column>
                                <Column field='pesoBruto' header='Peso Bruto'></Column>
                                <Column field='fobUS' header='FOB US'></Column>
                            </DataTable>
                        )
                        :
                        (
                            <span></span>
                        )
                }


            </Panel>


        </div>
    )
}

export default VistaPrevia