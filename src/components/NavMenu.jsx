import React, { useEffect, useState } from 'react';
import { Link, useHistory } from 'react-router-dom';
import { auth } from '../firebaseconfig';

const NavMenu = () => {

    const historial = useHistory()
    const [user, setUser] = useState(null)

    useEffect(() => {
        auth.onAuthStateChanged((user) => {
            if (user) {
                setUser(user.email)
            }
        })
    }, [])


    const cerrarSesion = () => {
        auth.signOut()
        setUser(null)
        historial.push('/')
        window.location.reload()
    }

    return (
        <div>
            <nav className='navbar navbar-expand-lg navbar-light bg-light container'>
                <ul className='navbar-nav mr-auto'>
                    <li className='nav-item'>
                        <Link className='nav-link' to='/'>Inicio</Link>
                    </li>
                    {
                        user ?
                            (
                                <li className='nav-item'>
                                    <Link className='nav-link' to='/dashboard'>Dashboard</Link>
                                </li>
                            )
                            :
                            (
                                <span></span>
                            )
                    }
                </ul>
                <ul className='navbar-nav'>
                    {
                        user ?
                            (
                                <li className='nav-item'>
                                    <Link to='/' className='nav-link' onClick={cerrarSesion}>Cerrar Sesión</Link>
                                </li>
                            )
                            :
                            (
                                <span></span>
                            )
                    }
                </ul>
            </nav>

        </div>
    )
}

export default NavMenu