import React from 'react';
import { auth } from '../firebaseconfig';

const RecepcionUser = (props) => {

    const { email } = props

    return (
        <div className='row mt-5'>
            <div className='col'></div>
            <div className='col mt-5'>
                <h5 className='text-center'>Bienvenido. Sesión iniciada con el correo {email} </h5>
            </div>
            <div className='col'></div>
        </div>
    )
}

export default RecepcionUser