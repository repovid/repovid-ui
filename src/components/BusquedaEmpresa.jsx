import React, { useState } from 'react';
import { useHistory } from 'react-router-dom';

const BusquedaEmpresa = () => {

    const historial = useHistory()

    const [codigo, setCodigo] = useState('')
    const [anio, setAnio] = useState('')
    const [pais, setPais] = useState('')
    const [errors, setErrors] = useState('')

    const buscarDatos = async (e) => {
        historial.push({
            pathname: '/vistaprevia',
            state: {
                codigo,
                anio,
                pais,
                type: 'empresa'
            }
        })
    }


    const validarDatos = (e) => {
        e.preventDefault()
        if (codigo === '' || anio === '' || anio === 'Seleccione año' || pais === '' || pais === 'Seleccione pais') {
            setErrors('Uno de los parámetros no está definido. Recuerde rellenar todos los datos.')
        }
        else {
            setErrors('')
            buscarDatos(e)
        }
    }

    return (
        <div className='container text-center'>
            <div className='row'>
                <div className='col'></div>
                <div className='col mt-5'>
                    <h3>Busqueda por identificador de empresa</h3>

                    <form className='form-group' onSubmit={validarDatos}>
                        <h5 className='mt-4'>Identificador Empresa:</h5>
                        <input className='form-control'
                            type="text"
                            onChange={(e) => setCodigo(e.target.value)} />

                        <h5 className='mt-4'>Año:</h5>
                        <select className='form-select'
                            aria-label='anio'
                            onChange={(e) => setAnio(e.target.value)}>
                            <option selected>Seleccione año</option>
                            <option value="2020">2020</option>
                            <option value="2019">2019</option>
                            <option value="2018">2018</option>
                            <option value="2017">2017</option>
                            <option value="2016">2016</option>
                            <option value="2015">2015</option>
                            <option value="2014">2014</option>
                            <option value="2013">2013</option>
                            <option value="2012">2012</option>
                            <option value="2011">2011</option>
                            <option value="2010">2010</option>
                        </select>

                        <h5 className='mt-4'>Pais:</h5>
                        <select className='form-select'
                            aria-label='pais'
                            onChange={(e) => setPais(e.target.value)}>
                            <option selected>Seleccione pais</option>
                            <option value="CHILE">Chile</option>
                            <option value="ARGENTINA">Argentina</option>
                            <option value="BOLIVIA">Bolivia</option>
                            <option value="COLOMBIA">Colombia</option>
                            <option value="COSTA RICA">Costa Rica</option>
                            <option value="ECUADOR">Ecuador</option>
                            <option value="GUATEMALA">Guatemala</option>
                            <option value="MEXICO">México</option>
                            <option value="PANAMA">Panamá</option>
                            <option value="PARAGUAY">Paraguay</option>
                            <option value="PERU">Perú</option>
                            <option value="URUGUAY">Uruguay</option>
                            <option value="VENEZUELA">Venezuela</option>
                        </select>

                        <input className='btn btn-primary btn-block mt-4'
                            value='Buscar'
                            type="submit" />
                    </form>
                    <h5 className='mt-3'>{errors}</h5>
                </div>
                <div className='col'></div>
            </div>
        </div>
    )
}

export default BusquedaEmpresa