import React, { useState } from 'react';
import axios from 'axios';

const ImportarDatos = () => {

    const [file, setFile] = useState('')
    const [pais, setPais] = useState('')
    const [errors, setErrors] = useState('')
    const [mensaje, setMensaje] = useState('')


    const importar = async (e) => {
        console.log("valor de pais", pais)
        setMensaje('Importando datos, por favor espere.')
        const data = new FormData();
        data.append('excel',file)
        data.append('paisOrigen',pais)
        await axios
            .post('http://3.235.143.253:8080/import/document', data
            ).then(res => {
                if (res.data == true) {
                    setMensaje('Datos importados correctamente.')
                } else {
                    setMensaje('Los datos no han podido ser importados.')
                }
                console.log(res.data)
            })
    }

    const validarDatos = (e) => {
        if (file === '' || pais === '' || pais === 'Seleccione pais') {
            setErrors('No ha seleccionado ningún archivo o no se ha especificado ningún país')
        }
        else {
            setErrors('')
            importar(e)
        }
    }

    console.log(pais)

    return (
        <div className='text-center mt-5'>
            <div className='mt-5'>
                <form className='form-group'>
                    <h5>Seleccione el archivo xlsx a subir:</h5>
                    <input type="file" name='excel' onChange={(e) => setFile(e.target.files[0])} />
                    <h5 className='mt-4'>Pais:</h5>
                    <select className='form-select'
                        aria-label='pais'
                        onChange={(e) => setPais(e.target.value)}>
                        <option selected>Seleccione pais</option>
                        <option value="CHILE">Chile</option>
                        <option value="ARGENTINA">Argentina</option>
                        <option value="BOLIVIA">Bolivia</option>
                        <option value="COLOMBIA">Colombia</option>
                        <option value="COSTA RICA">Costa Rica</option>
                        <option value="ECUADOR">Ecuador</option>
                        <option value="GUATEMALA">Guatemala</option>
                        <option value="MEXICO">México</option>
                        <option value="PANAMA">Panamá</option>
                        <option value="PARAGUAY">Paraguay</option>
                        <option value="PERU">Perú</option>
                        <option value="URUGUAY">Uruguay</option>
                        <option value="VENEZUELA">Venezuela</option>
                    </select>
                </form>

            </div>

            <button className='btn btn-primary mt-4' onClick={validarDatos}>Importar Datos</button>
            <h5 className='mt-3'>{errors}</h5>

            <div><h5 className='mt-5'>{mensaje}</h5></div>
        </div>
    )
}

export default ImportarDatos