import React from 'react';
import { useHistory } from 'react-router-dom';

const Dashboard = () => {

    const historial = useHistory()

    const BuscarEmpresa = () => {
        historial.push('busqueda_empresa')
    }

    const BuscarCodigo = () => {
        historial.push('busqueda_codigo')
    }

    const ImportarDatos = () => {
        historial.push('importar')
    }


    return (
        <div className='container'>
            <div className='row mt-3'>
                <div className='col'>
                    <h3>Dashboard</h3>
                </div>
            </div>
            <button className='btn btn-primary mt-2 float-right' onClick={ImportarDatos}>Importar Datos</button>
            <h5 className='text-center mt-5'>Repovid provee de dos formatos para buscar datos. </h5>
            <h5 className='text-center'>Buscar datos según partida arancelaria y buscar datos segun identificador de empresa. </h5>
            <div className='row'>
                <div className='col mt-5 text-center'>
                    <h6>Buscar utilizando la partida arancelaria,</h6>
                    <h6>el pais y el año</h6>
                    <button className='btn btn-info mt-3' onClick={BuscarCodigo}>Partida Arancelaria</button>
                </div>
                <div className='col mt-5 text-center'>
                    <h6>Buscar utilizando el identificador de la empresa,</h6>
                    <h6>el pais y el año</h6>
                    <button className='btn btn-info mt-3' onClick={BuscarEmpresa}>Empresa</button>
                </div>
            </div>
        </div>
    )
}

export default Dashboard